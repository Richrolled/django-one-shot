from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoForm, ItemForm

# Create your views here.

def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)

def show_todo(request, id):
    a_todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": a_todo,
        }
    return render(request, "todos/detail.html", context)

def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(show_todo, form.save().id)
    else:
        form = TodoForm()
    context = {
        "form":form,
    }
    return render(request, "todos/create.html", context)

def edit_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=todo)
    context = {
        "form": form,
        "todo": todo,
    }
    return render(request, "todos/edit.html", context)

def delete_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        item = ItemForm()
    context = {
        "item":item,
    }
    return render(request, "todos/createitem.html", context)

def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            new = form.save()
            return redirect("todo_list_detail", id=new.list.id)
    else:
        form = ItemForm(instance=item)
    context = {
        "form": form,
        "item": item,
    }
    return render(request, "todos/edititem.html", context)
